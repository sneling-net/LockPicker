package net.sneling.lockpicker.timers;

import net.sneling.lockpicker.LockPicker;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

/**
 * Created by Sneling on 9/26/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class CountdownTimer implements Runnable {

    private int level;
    private int counter;
    private Player player;
    private BukkitTask task;

    public CountdownTimer(Player p, int level, int counter) {
        this.player = p;
        this.level = level;
        this.counter = counter;

        task = Bukkit.getScheduler().runTaskTimer(LockPicker.getInstance(), this, 0, 20);
    }

    public void cancel(){
        task.cancel();
        player.setLevel(level);
    }

    public int getLevel() {
        return level;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public void run() {
        if(counter <= 0) {
            cancel();
            return;
        }

        player.setLevel(counter);
        counter--;
    }

}
