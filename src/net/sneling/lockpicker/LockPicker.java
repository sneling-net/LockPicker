package net.sneling.lockpicker;

import net.sneling.lockpicker.commands.LockPickerCommand;
import net.sneling.lockpicker.configs.Config;
import net.sneling.lockpicker.configs.KeyConfig;
import net.sneling.lockpicker.lang.Lang;
import net.sneling.lockpicker.listeners.LockpickListener;
import net.sneling.lockpicker.perm.Perm;
import net.sneling.snelapi.plugin.SnelPlugin;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class LockPicker extends SnelPlugin {

    private static LockPicker instance;

    private Config config;
    private KeyConfig keyConfig;

    private Perm perm;
    private Lang lang;

    public LockPicker() {
        super("LockPicker", "LockP", "1.0A", "sneling-lockpicker");

        instance = this;

        register();

        config = new Config();
        keyConfig = new KeyConfig();


        this.lang = new Lang();
    }

    public void onPluginLoad(){
        this.perm = new Perm();
    }

    public void onPluginEnable(){
        registerCommand(new LockPickerCommand());

        getServer().getPluginManager().registerEvents(new LockpickListener(), this);
    }

    public static LockPicker getInstance() {
        return instance;
    }

    public Config getPluginConfig() {
        return config;
    }

    public KeyConfig getKeyConfig() {
        return keyConfig;
    }
}