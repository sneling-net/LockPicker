package net.sneling.lockpicker.lang;

import net.sneling.lockpicker.LockPicker;
import net.sneling.snelapi.language.SnelLanguage;
import net.sneling.snelapi.language.SnelLanguageParent;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Lang extends SnelLanguageParent {

    public Lang() {
        super("lockpicker", "lang", LockPicker.getInstance());
    }

    public static SnelLanguage
    COMMAND_GIVE_DESCRIPTION = new SnelLanguage("Give LockPicking Keys", false),
    COMMAND_GIVE_NOT_FOUND = new SnelLanguage("&cCould not find a key with this id"),
    COMMAND_GIVE_SUCCESS_SENDER = new SnelLanguage("&aYou gave %AMOUNT% lockpicker keys #%ID% to %PLAYER%"),
    COMMAND_GIVE_SUCCESS_RECEIVER = new SnelLanguage("&aYou received %AMOUNT% lockpicking keys"),

    COMMAND_REGISTER_EMPTY_HAND = new SnelLanguage("&cYou must have an item in hand to continue!"),
    COMMANG_REGISTER_COMPLETE = new SnelLanguage("&aYou registered a new lockpick key with id %ID%!"),

    LOCKPICK_BEGIN = new SnelLanguage("&aStarting lockpicking process...\n &aETA: %TIME% seconds\n &aSuccess Chance: %CHANCE%%"),
    LOCKPICK_COOLDOWN = new SnelLanguage("&cYou're already lockpicking this! %TIME% seconds left");
//    LOCKPICK_PROTECTION = new SnelLanguage("&This is protected by a region. ");

}
