package net.sneling.lockpicker.perm;

import net.sneling.lockpicker.LockPicker;
import net.sneling.snelapi.permission.SnelPermission;
import net.sneling.snelapi.permission.SnelPermissionParent;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Perm extends SnelPermissionParent {

    public Perm() {
        super("lockpicker", LockPicker.getInstance());
    }

    public static SnelPermission
    COMMAND_GIVE = new SnelPermission("Allows the Permissible to give LockPicking Keys"),
    COMMAND_REGISTER_KEY = new SnelPermission("Allows the Permissible to register keys"),

    KEY_USE = new SnelPermission("Default value for LockPicking keys");

}
