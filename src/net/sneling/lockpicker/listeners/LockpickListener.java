package net.sneling.lockpicker.listeners;

import net.sneling.lockpicker.Actions;
import net.sneling.lockpicker.LockPicker;
import net.sneling.lockpicker.keys.Key;
import net.sneling.lockpicker.keys.KeyHandler;
import net.sneling.lockpicker.lang.Lang;
import net.sneling.lockpicker.timers.CountdownTimer;
import net.sneling.lockpicker.utils.LocketteUtil;
import net.sneling.lockpicker.utils.LockpickUtil;
import net.sneling.snelapi.cooldown.CooldownManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class LockpickListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onSignClick(PlayerInteractEvent e){
        Player clicker = e.getPlayer();

        if(e.getClickedBlock() == null || e.getClickedBlock().getType() != Material.WALL_SIGN)
            return;

//        if(e.isCancelled() && LockPicker.getInstance().getPluginConfig().getConfig().getBoolean("protection-override", true)){
//            clicker.sendMessage(Lang.INTERACT_PROTECTION.get());
//            return;
//        }

        UUID owner = LocketteUtil.getOwner(e.getClickedBlock());

        if(owner == null)
            return;

        if(owner.equals(clicker.getUniqueId()))
            return;

        Key currentKey = KeyHandler.getKey(clicker.getItemInHand());

        if(currentKey == null)
            return;

        if(CooldownManager.getTimeLeft(clicker, Actions.LOCKPICK) > 0){ // TODO Allow cancel (Player will still loose his key)
            clicker.sendMessage(Lang.LOCKPICK_COOLDOWN.get().replaceAll("%TIME%", CooldownManager.getTimeLeft(clicker, Actions.LOCKPICK) / 20 + ""));
            return;
        }

        // Starting lockpick process
        e.setCancelled(true);
        CooldownManager.registerCooldown(clicker, Actions.LOCKPICK, currentKey.getTime(), CooldownManager.OverwriteOption.STATIC);
        clicker.sendMessage(Lang.LOCKPICK_BEGIN.get().replaceAll("%TIME%", (double) currentKey.getTime() / 20 + "").replaceAll("%CHANCE%", currentKey.getChance() + ""));

        // TODO Start Lockpick

        LockpickUtil.playOngoingSound(e.getClickedBlock().getLocation());

        ItemStack item = clicker.getItemInHand();
        item.setAmount(item.getAmount() - 1);
        clicker.setItemInHand(item);

        new CountdownTimer(clicker, clicker.getLevel(), (int) (currentKey.getTime() / 20));

        Bukkit.getScheduler().runTaskLater(LockPicker.getInstance(), new Runnable() {
            @Override
            public void run() {
                if(LockpickUtil.isSuccessful(currentKey.getChance())){
                    LockpickUtil.playSuccessSound(e.getClickedBlock().getLocation());
                    e.getClickedBlock().setType(Material.AIR);
                    e.getClickedBlock().getWorld().dropItem(e.getClickedBlock().getLocation(), new ItemStack(Material.SIGN));
                }else{
                    LockpickUtil.playFailSound(e.getClickedBlock().getLocation());
                }
            }
        }, currentKey.getTime());
    }

}
