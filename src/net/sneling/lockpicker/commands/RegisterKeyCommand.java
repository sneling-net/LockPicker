package net.sneling.lockpicker.commands;

import net.sneling.lockpicker.LockPicker;
import net.sneling.lockpicker.keys.Key;
import net.sneling.lockpicker.lang.Lang;
import net.sneling.lockpicker.perm.Perm;
import net.sneling.snelapi.a.internal.lang.APILang;
import net.sneling.snelapi.commands.SnelCommand;
import net.sneling.snelapi.commands.arg.ArgInfo;
import net.sneling.snelapi.commands.arg.ArgRequirement;
import net.sneling.snelapi.commands.arg.ArgType;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Sneling on 9/24/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class RegisterKeyCommand extends SnelCommand {

    protected RegisterKeyCommand() {
        super("register");

        setPermission(Perm.COMMAND_REGISTER_KEY);
        setForcePlayerExecuter(true);
        setDescription("Pre-register an item as a key");

        addArgument(new ArgInfo("chance", ArgType.MENDATORY, ArgRequirement.DOUBLE));
        addArgument(new ArgInfo("time", ArgType.MENDATORY, ArgRequirement.INTEGER));
        addArgument(new ArgInfo("permission", ArgType.OPTIONAL).setDefault("default"));
    }

    public void execute(CommandSender sender, String[] args){
        Player p = (Player) sender;
        double chance = Double.parseDouble(args[0]);
        long time = Long.parseLong(args[1]);
        String node = args[2];

        if(p.getInventory().getItemInMainHand() == null || p.getInventory().getItemInMainHand().getType() == Material.AIR){
            p.sendMessage(Lang.COMMAND_REGISTER_EMPTY_HAND.get());
            return;
        }

        ItemStack item = p.getItemInHand();
        item.setAmount(1);

        Key key = new Key(item, chance, time, node);

        try {
            int id = LockPicker.getInstance().getKeyConfig().registerKey(key);

            p.sendMessage(Lang.COMMANG_REGISTER_COMPLETE.get().replaceAll("%ID%", id + ""));
        }catch (Exception e){
            p.sendMessage(APILang.GENERAL_ERROR.get());
            e.printStackTrace();
        }
    }

}
