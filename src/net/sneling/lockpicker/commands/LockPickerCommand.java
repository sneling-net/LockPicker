package net.sneling.lockpicker.commands;

import net.sneling.snelapi.commands.SnelCommand;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class LockPickerCommand extends SnelCommand {

    private GiveCommand giveCommand;
    private RegisterKeyCommand registerKeyCommand;

    public LockPickerCommand() {
        super("lockpicker");

        setDescription("LockPicker's main command");

        addChild(giveCommand = new GiveCommand());
        addChild(registerKeyCommand = new RegisterKeyCommand());
    }

}