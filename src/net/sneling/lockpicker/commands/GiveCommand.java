package net.sneling.lockpicker.commands;

import net.sneling.lockpicker.keys.Key;
import net.sneling.lockpicker.keys.KeyHandler;
import net.sneling.lockpicker.lang.Lang;
import net.sneling.lockpicker.perm.Perm;
import net.sneling.snelapi.commands.SnelCommand;
import net.sneling.snelapi.commands.arg.ArgInfo;
import net.sneling.snelapi.commands.arg.ArgRequirement;
import net.sneling.snelapi.commands.arg.ArgType;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class GiveCommand extends SnelCommand{

    protected GiveCommand() {
        super("give");

        setPermission(Perm.COMMAND_GIVE);
        setDescription(Lang.COMMAND_GIVE_DESCRIPTION.get());

        addArgument(new ArgInfo("player", ArgType.MENDATORY, ArgRequirement.PLAYER));
        addArgument(new ArgInfo("id", ArgType.MENDATORY, ArgRequirement.INTEGER));
        addArgument(new ArgInfo("amount", ArgType.OPTIONAL, ArgRequirement.INTEGER).setDefault("1"));
    }

    public void execute(CommandSender sender, String[] args){
        Player p = Bukkit.getPlayer(args[0]);
        int id = Integer.parseInt(args[1]);
        int amount = Integer.parseInt(args[2]);

        Key key = KeyHandler.getKey(id);

        if(key == null){
            sender.sendMessage(Lang.COMMAND_GIVE_NOT_FOUND.get());
            return;
        }

        for(int i = 0; i < amount; i++)
            p.getInventory().addItem(key.getItemStack());

        sender.sendMessage(Lang.COMMAND_GIVE_SUCCESS_SENDER.get().replaceAll("%ID%", id + "").replaceAll("%PLAYER%", p.getName()).replaceAll("%AMOUNT%", amount + ""));
        p.sendMessage(Lang.COMMAND_GIVE_SUCCESS_RECEIVER.get().replaceAll("%ID%", id + "").replaceAll("%PLAYER%", p.getName()).replaceAll("%AMOUNT%", amount + ""));
    }

}
