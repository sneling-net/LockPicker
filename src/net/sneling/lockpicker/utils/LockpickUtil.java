package net.sneling.lockpicker.utils;

import org.bukkit.Location;
import org.bukkit.Sound;

import java.util.Random;

/**
 * Created by Sneling on 9/25/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class LockpickUtil {

    public static void playOngoingSound(Location l){
        l.getWorld().playSound(l, Sound.BLOCK_ANVIL_HIT, 1, .7F);
        l.getWorld().playSound(l, Sound.BLOCK_CHEST_LOCKED, 1, .9F);
        l.getWorld().playSound(l, Sound.BLOCK_ANVIL_PLACE, .2F, .7F);
    }


    public static void playFailSound(Location l){
        l.getWorld().playSound(l, Sound.BLOCK_CHEST_CLOSE, 1, .5F);
        l.getWorld().playSound(l, Sound.BLOCK_GLASS_BREAK, 1, .6F);
    }

    public static void playSuccessSound(Location l){
        l.getWorld().playSound(l, Sound.BLOCK_CHEST_OPEN, 1, 1);
        l.getWorld().playSound(l, Sound.ENTITY_PLAYER_LEVELUP, .1F, 1);
        l.getWorld().playSound(l, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, .4F);
    }

    /**
     *
     * @param chance 0<chance<100
     * @return True if a randomly generated number is lower or equal to <code>chance</code>
     */
    public static boolean isSuccessful(double chance){
        if(chance <= 0) // Ease
            return false;

        if(chance >= 100)
            return true;

        Random r = new Random();
        double decimals = r.nextDouble();
        int units = r.nextInt(100);

        return (units + decimals) <= chance;
    }


}
