package net.sneling.lockpicker.utils;

import org.bukkit.block.Block;
import org.yi.acru.bukkit.Lockette.Lockette;

import java.util.UUID;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class LocketteUtil {

    public static UUID getOwner(Block b){
        return Lockette.getProtectedOwnerUUID(b);
    }

}
