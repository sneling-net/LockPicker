package net.sneling.lockpicker;

import net.sneling.snelapi.cooldown.CooldownAction;

/**
 * Created by Sneling on 9/25/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public enum Actions implements CooldownAction {
    LOCKPICK


}
