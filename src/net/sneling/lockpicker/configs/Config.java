package net.sneling.lockpicker.configs;

import net.sneling.lockpicker.LockPicker;
import net.sneling.snelapi.file.yml.SnelYMLConfig;

/**
 * Created by Sneling on 9/17/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Config extends SnelYMLConfig {

    public Config() {
        super(LockPicker.getInstance(), "config", true);
    }

}
