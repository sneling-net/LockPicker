package net.sneling.lockpicker.configs;

import net.sneling.lockpicker.LockPicker;
import net.sneling.lockpicker.keys.Key;
import net.sneling.lockpicker.keys.KeyHandler;
import net.sneling.snelapi.file.yml.SnelYMLConfig;
import net.sneling.snelapi.logger.Logger;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Sneling on 9/24/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class KeyConfig extends SnelYMLConfig {

    /*
    keys:
      id: INT
        item: ITEMSTACK
        chance: DOUBLE
        time: LONG
        permission: STRING
     */

    public KeyConfig() {
        super(LockPicker.getInstance(), "keys", false);
    }

    public HashMap<Integer, Key> getKeys(){
        HashMap<Integer, Key> keys = new HashMap<>();

        if(!getConfig().isSet("keys"))
            return keys;

        for(String key: getConfig().getConfigurationSection("keys").getKeys(false)){
            int id = Integer.parseInt(key.replaceAll("'", ""));

            keys.put(id, getKey(id));
        }

        return keys;
    }

    public Key getKey(int id){
        Logger.debug("Loading key #" + id, LockPicker.getInstance());

        return new Key(
                getConfig().getItemStack("keys." + id + ".item"),
                getConfig().getDouble("keys." + id + ".chance", 50),
                getConfig().getLong("keys." + id + ".time", 100),
                getConfig().getString("keys." + id + ".permission", "default"));
    }

    public void setKey(int id, Key key) throws IOException {
        String path = "keys." + id + ".";
        getConfig().set(path + "item", key.getItemStack());
        getConfig().set(path + "chance", key.getChance());
        getConfig().set(path + "time", key.getTime());
        getConfig().set(path + "permission", key.getPermissionNode());

        save();
    }

    public int registerKey(Key key) throws IOException {
        int id = getRandomID();

        setKey(id, key);

        return id;
    }

    private int getRandomID(){
        for(int i = 0; i < 100; i++)
            if(!getConfig().isSet("keys." + i))
                return i;

        return 101;
    }

    public void postLoad(){
        KeyHandler.loadKeys();
    }

}
