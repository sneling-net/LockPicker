package net.sneling.lockpicker.keys;

import net.sneling.lockpicker.perm.Perm;
import net.sneling.snelapi.permission.PermChecker;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permissible;

/**
 * Created by Sneling on 9/24/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class Key {

    private ItemStack itemStack;
    private double chance;
    private long time;
    private String permissionNode;

    public Key(ItemStack item, double chance, long time) {
        this(item, chance, time, "default");
    }

    public Key(ItemStack item, double chance, long time, String permissionNode) {
        this.itemStack = item;
        this.chance = chance;
        this.time = time;
        this.permissionNode = permissionNode;
    }

    public String getPermissionNode() {
        return permissionNode;
    }

    public double getChance() {
        return chance;
    }

    public long getTime() {
        return time;
    }

    public boolean canUse(Permissible p){
        return PermChecker.has(p, Perm.KEY_USE.getID() + "." + permissionNode);
    }

    public ItemStack getItemStack() {
        return itemStack;
    }
}
