package net.sneling.lockpicker.keys;

import net.sneling.lockpicker.LockPicker;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * Created by Sneling on 9/24/2016 for LockPicker.
 * <p>
 * Copyright © 2016 - Now Sneling
 * <p>
 * You are not allowed to copy any of the code contained in this file.
 * If you have any questions about this, what it means, and in which circumstances you're allowed to use this code,
 * send an email to: contact@sneling.net
 */
public class KeyHandler {

    private static HashMap<Integer, Key> keys = new HashMap<>();

    public static void loadKeys(){
        keys = LockPicker.getInstance().getKeyConfig().getKeys();
    }

    public static Key getKey(int id){
        return keys.get(id);
    }

    public static Key getKey(ItemStack itemStack){
        if(itemStack == null)
            return null;

        for(Key key: keys.values())
            if(key.getItemStack().isSimilar(itemStack))
                return key;

        return null;
    }

}